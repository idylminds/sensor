#ifndef _SENSOR_DOT_H_
#define _SENSOR_DOT_H_

#include <Arduino.h>

/**
 * message types
 */
#define PUT_NOOP          0
#define PUT_TEMPERATURE   1
#define PUT_PRESSURE      2
#define PUT_HUMIDITY      3
#define PUT_LIGHT         4
#define PUT_SOUND         5
#define PUT_MESSAGE       6
#define PUT_TRIGGER       7
#define PUT_TRIGGER       8 

#define GET_NOOP          100
#define GET_TEMPERATURE   101
#define GET_PRESSURE      102
#define GET_HUMIDITY      103
#define GET_LIGHT         104
#define GET_SOUND         105
#define GET_MESSAGE       106
#define GET_TRIGGER       107

/**
 * shared constants
 */
#define SENSOR_ADDRESS_BYTES { 0x71, 0xCD, 0xAB, 0xCD, 0xAB }
#define SENSOR_ADDRESS_UINT64 0xABCDABCD71LL

typedef struct
{
  double farenheit;
  unsigned long ticks;
} Temperature;

typedef struct
{
  double pressure;
  double altitude;
  unsigned long ticks;
} Pressure;

typedef struct
{
  byte absolute;
  byte relative;
  byte specific;
  unsigned long ticks;
} Humidity;

typedef struct
{
  double lux;
  unsigned long ticks;
} Light;

typedef struct
{
  double db;
  unsigned long ticks;
} Sound;

typedef struct
{
  byte index;
  unsigned long ticks;
} UltraViolet;

typedef struct
{
  bool state;
  unsigned long ticks;
} Trigger;

typedef struct
{
  byte type;
  byte buf[24];
  unsigned long ticks;
} Message;

union Payload
{
  Message message;
  Temperature temperature;
  Pressure pressure;
  Humidity humidity;
  Light light;
  Sound sound;
  UltraViolet uv;
  Trigger trigger;
};

typedef struct
{
  byte channel;
  byte type;
  union Payload payload;
} Packet;

#endif // _SENSOR_DOT_H_
